package edu.westga.letterchart.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;

/**
 * A CharacterHistogram is a histogram of letter values read from a text file.
 * 
 * @author lewisb
 *
 */
public class CharacterHistogram {
	HashMap<Character, Integer> charValues;


	/**
	 * Creates an empty CharacterHistogram.
	 */
	public CharacterHistogram() {
		this.charValues = new HashMap<Character, Integer>();

	}

	/**
	 * Gets the count for a given character.
	 *
	 * @param character
	 *            the character we're interested in
	 * @return the number of times it appears in the histogram
	 */
	public int getCountFor(char character) {
		if (!this.charValues.containsKey(character)) {
			this.charValues.put(character, 0);
		}
		return this.charValues.get(character);

	}

	/**
	 * Increases (by one) the count for the given character. Ignores
	 * non-alphabetic characters.
	 *
	 * @param character
	 *            the character for which we should increase the count
	 */
	private void increaseCountFor(char character) {
		int newValue = this.getCountFor(character) + 1;
		this.charValues.replace(character, newValue);

	}

	/**
	 * Clears this CharacterHistogram and loads in the new character data
	 *
	 * @param textFile
	 *            the file to open
	 * @throws FileNotFoundException
	 *             if the file cannot be found
	 * @throws IOException
	 *             if an unexpected I/O problem occurs
	 * @throws IllegalArgumentException
	 *             if the file is null
	 */
	public void load(File textFile) throws FileNotFoundException, IOException {
		if (textFile == null) {
			throw new IllegalArgumentException("Null file");
		}

		this.charValues.clear();
		Scanner scan = new Scanner(textFile);
		while (scan.hasNext())
		{
			String line = scan.nextLine();
			line.toLowerCase();
			this.processLine(line);
		}
		scan.close();

	}

	private void processLine(String line) {
		for (int counter = 0; counter < line.length(); counter++) {
			char aChar = line.charAt(counter);
			aChar = Character.toLowerCase(aChar);
			if (this.isALetter(aChar)) {
				this.increaseCountFor(aChar);
			}
		}
	}

	private boolean isALetter(char aLetter) {
		return (aLetter >= 'a' && aLetter <= 'z') || (aLetter >= 'A' && aLetter <= 'Z');
	}

	/**
	 * Data property, suitable for consumption by a PieChart.
	 *
	 * @return the histogram as pie char data
	 */
	public ObservableList<PieChart.Data> dataProperty() {
		ObservableList<Data> letterData = FXCollections.observableArrayList();
		Collection<Character> charKeys = this.charValues.keySet();

		for (Character character : charKeys) {
			letterData.add(new PieChart.Data("" + character, this.charValues.get(character)));
		}

		return letterData;
	}
}
