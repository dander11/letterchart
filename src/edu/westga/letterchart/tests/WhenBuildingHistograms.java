/**
 *
 */
package edu.westga.letterchart.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javafx.scene.chart.PieChart;

import org.junit.Before;
import org.junit.Test;

import edu.westga.letterchart.model.CharacterHistogram;

/**
 * @author dander11
 *
 */
public class WhenBuildingHistograms {
	private CharacterHistogram histogram;
	private File nullFile;
	private File emptyFile;
	private File singleLetterFile;
	private File allLettersFile;
	private File fileWithSevenAChar;
	private File helloWorldFile;
	private File puctuationFile;
	private File puctuationAndLettersFile;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.histogram = new CharacterHistogram();
		this.emptyFile = new File("emptyFile.txt");
		this.singleLetterFile = new File("singleLettterFile.txt");
		this.allLettersFile = new File("allLettersFiles.txt");
		this.fileWithSevenAChar = new File("aCharSevenTimes.txt");
		this.helloWorldFile = new File("hello world.txt");
		this.puctuationFile = new File("punctuationFile.txt");
		this.puctuationAndLettersFile = new File("punctuationAndLettersFile.txt");

	}

	@Test(expected = IllegalArgumentException.class)
	public void nullTextFileShouldThrowException() throws FileNotFoundException, IOException {
		this.histogram.load(this.nullFile);
	}

	@Test
	public void emptyFileShouldHaveNoData() throws FileNotFoundException, IOException {
		this.histogram.load(this.emptyFile);
		assertTrue(this.histogram.dataProperty().isEmpty());
	}

	@Test
	public void singleLetterFileShouldHaveOneEntryWithCountOfOne() throws FileNotFoundException, IOException {
		this.histogram.load(this.singleLetterFile);
		assertTrue(this.histogram.dataProperty().size() == 1 && this.histogram.dataProperty().get(0).getPieValue() == 1.0);
	}

	@Test
	public void fileWithOneOfEachLetterShouldHaveAppropriateHistogram() throws FileNotFoundException, IOException {
		this.histogram.load(this.allLettersFile);
		for (PieChart.Data pieValue : this.histogram.dataProperty()) {
			if(pieValue.getPieValue() != 1.0) {
				fail("One of the letters was not counted once and only once.");
			}
		}
		assertEquals(26, this.histogram.dataProperty().size());
	}

	@Test
	public void fileWithOneRepeatingLetterShouldOnlyCountThatLetter() throws FileNotFoundException, IOException {
		this.histogram.load(this.fileWithSevenAChar);
		
		assertTrue(this.histogram.dataProperty().size() == 1 && this.histogram.dataProperty().get(0).getPieValue() == 7.0);
	}
	
	@Test
	public void fileWithManyOfSomeLettersShouldHaveAppropriateHistogram () throws FileNotFoundException, IOException {
		this.histogram.load(this.helloWorldFile);
		int numOfHChar = 1;
		boolean correctNumOfH = false;
		int numOfEChar = 1;
		boolean correctNumOfE = false;
		int numOfLChar = 3;
		boolean correctNumOfL = false;
		int numOfOChar = 2;
		boolean correctNumOfO = false;
		int numOfWChar = 1;
		boolean correctNumOfW = false;
		int numOfRChar = 1;
		boolean correctNumOfR = false;
		int numOfDChar = 1;
		boolean correctNumOfD = false;
		
		for (PieChart.Data pieData : this.histogram.dataProperty()) {
			if (pieData.nameProperty().getValue().equals("h") && pieData.getPieValue() == numOfHChar) {
				correctNumOfH = true;
			}
			if (pieData.nameProperty().getValue().equals("e") && pieData.getPieValue() == numOfEChar) {
				correctNumOfE = true;
			}
			if (pieData.nameProperty().getValue().equals("l") && pieData.getPieValue() == numOfLChar) {
				correctNumOfL = true;
			}
			if (pieData.nameProperty().getValue().equals("o") && pieData.getPieValue() == numOfOChar) {
				correctNumOfO = true;
			}
			if (pieData.nameProperty().getValue().equals("w") && pieData.getPieValue() == numOfWChar) {
				correctNumOfW = true;
			}
			if (pieData.nameProperty().getValue().equals("r") && pieData.getPieValue() == numOfRChar) {
				correctNumOfR = true;
			}
			if (pieData.nameProperty().getValue().equals("d") && pieData.getPieValue() == numOfDChar) {
				correctNumOfD = true;
			}
		}
		assertTrue(correctNumOfH && correctNumOfE && correctNumOfL && correctNumOfO && correctNumOfW && correctNumOfR && correctNumOfD);
	}
	
	@Test
	public void fileWithOnlyPunctuationShouldHaveNoData() throws FileNotFoundException, IOException {
		this.histogram.load(this.puctuationFile);
		assertTrue(this.histogram.dataProperty().isEmpty());
	}
	
	@Test
	public void fileWithMixedLettersAndPunctuationShouldIgnorePunctuation() throws FileNotFoundException, IOException {
		this.histogram.load(this.puctuationAndLettersFile);
		assertEquals(10, this.histogram.dataProperty().size());
	} 
	
	
}
