/**
 *
 */
package edu.westga.letterchart.views;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import edu.westga.letterchart.model.CharacterHistogram;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller class for GUI.
 *
 * @author Kathryn Browning
 * @version January 13, 2015
 *
 */
public class LetterChartController {
	private CharacterHistogram charHistogram;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private Menu fileMenu;

	@FXML
	private MenuItem openMenu;

	@FXML
	private MenuItem closeMenu;

	@FXML
	private PieChart pieChart;

	@FXML
	void initialize() {
		this.charHistogram = new CharacterHistogram();

		assert this.fileMenu != null : "fx:id=\"fileMenu\" was not injected : check your FXML file 'MainGui.fxml'.";
		assert this.openMenu != null : "fx:id=\"openMenu\" was not injected : check your FXML file 'MainGui.fxml'.";
		assert this.closeMenu != null : "fx:id=\"closeMenu\" was not injected : check your FXML file 'MainGui.fxml'.";
		assert this.pieChart != null : "fx:id=\"pieChart\" was not injected : check your FXML file 'MainGui.fxml'.";

		this.menuFunctionality();
	}

	private void setChartData() {
		this.pieChart.setData(this.charHistogram.dataProperty());
	}

	private void menuFunctionality() {
		this.openMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				FileChooser fileChooser = new FileChooser();
				File histogramFile = fileChooser.showOpenDialog(new Stage());
				if (histogramFile != null) {
					try {
						LetterChartController.this.charHistogram.load(histogramFile);
						LetterChartController.this.setChartData();
					} catch (IOException e) {
						System.out.println("Loading file has failed.");
					}
				}
			}
		});
		this.closeMenu.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});
	}

}
